package pl.kkorobkow.equipy.common;

import org.aspectj.lang.JoinPoint;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class GeneralLoggingAspect {
    public void logInfoBefore(JoinPoint joinPoint, Logger logger) {
        Object[] args = joinPoint.getArgs();
        String message = String.format("Calling the method %s() with args: %s", joinPoint.getSignature().getName(),
                Arrays.toString(args));
        logger.info(message);
    }

//    public void logInfoAfter(JoinPoint joinPoint, Logger logger) {
//        String message = String.format("Method %s() executed", joinPoint.getSignature().getName());
//        logger.info(message);
//    }

    public void logError(JoinPoint joinPoint, Logger logger, Throwable error) {
        String message = String.format("Method %s() finished with error '%s'", joinPoint.getSignature().getName(), error);
        logger.warn(message);
    }

    public void logSuccess(JoinPoint joinPoint, Logger logger, Object result) {
        String message = String.format("Method %s() successfully returned value '%s %s'", joinPoint.getSignature().getName(),
                result.getClass(), result);
        logger.info(message);
    }
}
