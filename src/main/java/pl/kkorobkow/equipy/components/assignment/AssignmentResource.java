package pl.kkorobkow.equipy.components.assignment;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.kkorobkow.equipy.components.assignment.exception.InvalidAssignmentException;

import java.net.URI;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/api/assignments")
public class AssignmentResource {
    private final AssignmentService assignmentService;

    public AssignmentResource(AssignmentService assignmentService) {
        this.assignmentService = assignmentService;
    }

    @PostMapping("")
    ResponseEntity<AssignmentDto> save(@RequestBody AssignmentDto assignment) {
        if (assignment.getUserId() == null || assignment.getAssetId() == null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID wypożyczającego użytkownika oraz wypożyczanego" +
                    " zasobu nie mogą być puste");
        AssignmentDto savedAssignment;
        try {
            savedAssignment = assignmentService.save(assignment);
        } catch (InvalidAssignmentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedAssignment.getId())
                .toUri();
        return ResponseEntity.created(location).body(savedAssignment);
    }

    @PostMapping("/{id}/end")
    ResponseEntity<LocalDateTime> finishAssignment(@PathVariable Long id) {
        LocalDateTime endTime = assignmentService.finishAssignment(id);
        return ResponseEntity.ok(endTime);
    }
}
