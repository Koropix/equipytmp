package pl.kkorobkow.equipy.components.assignment.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
class AssignmentAspectUtil {
    @Pointcut("execution(* pl.kkorobkow.equipy.components.assignment.AssignmentResource.*(..))")
    void allAssignmentResourceMethods() {}
}
