package pl.kkorobkow.equipy.components.assignment.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Brak przypisania o takim ID")
public class AssignmentNotFoundException extends RuntimeException {
}
