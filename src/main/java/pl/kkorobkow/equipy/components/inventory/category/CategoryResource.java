package pl.kkorobkow.equipy.components.inventory.category;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/categories")
public class CategoryResource {
    private final CategoryService categoryService;

    CategoryResource(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/names")
    List<String> findAllNames() {
        return categoryService.findAllNames();
    }
}
