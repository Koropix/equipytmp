package pl.kkorobkow.equipy.components.user;

import org.springframework.stereotype.Service;
import pl.kkorobkow.equipy.components.user.dto.UserAssignmentDto;
import pl.kkorobkow.equipy.components.user.dto.UserDto;
import pl.kkorobkow.equipy.components.user.exception.DuplicatePeselException;
import pl.kkorobkow.equipy.components.user.exception.UserNotFoundException;
import pl.kkorobkow.equipy.components.user.mapper.UserAssignmentMapper;
import pl.kkorobkow.equipy.components.user.mapper.UserMapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
class UserService {

    private final UserRepository userRepository;

    UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    List<UserDto> findAll() {
        List<User> usersList = userRepository.findAll();
        return mapUsersListToDtoList(usersList);
    }

    List<UserDto> findByLastNameFragment(String lastNameFragment) {
        List<User> usersList = userRepository.findAllByLastNameContainsIgnoreCase(lastNameFragment);
        return mapUsersListToDtoList(usersList);
    }

    private List<UserDto> mapUsersListToDtoList(List<User> usersList) {
        return usersList.stream()
                .map(UserMapper::toDto)
                .collect(Collectors.toList());
    }

    Optional<UserDto> findById(Long id) {
        return userRepository.findById(id).map(UserMapper::toDto);
    }

    UserDto save(UserDto user) {
        Optional<User> userByPesel = userRepository.findByPesel(user.getPesel());
        userByPesel.ifPresent(u -> {
            throw new DuplicatePeselException();
        });
        return mapAndSaveUser(user);
    }

    UserDto update(UserDto user) {
        Optional<User> userByPesel = userRepository.findByPesel(user.getPesel());
        userByPesel.ifPresent(u -> {
            if (!u.getId().equals(user.getId())) {
                throw new DuplicatePeselException();
            }
        });
        return mapAndSaveUser(user);
    }

    private UserDto mapAndSaveUser(UserDto user) {
        User userEntity = UserMapper.toEntity(user);
        User savedUser = userRepository.save(userEntity);
        return UserMapper.toDto(savedUser);
    }

    List<UserAssignmentDto> getUserAssignments(Long userId) {
        return userRepository.findById(userId)
                .map(User::getAssignments)
                .orElseThrow(UserNotFoundException::new)
                .stream()
                .map(UserAssignmentMapper::toDto)
                .collect(Collectors.toList());
    }
}
