CREATE TABLE user
(
    id         BIGINT AUTO_INCREMENT NOT NULL,
    first_name VARCHAR(255)          NULL,
    last_name  VARCHAR(255)          NULL,
    pesel      VARCHAR(255)          NULL,
    CONSTRAINT pk_user PRIMARY KEY (id)
);

ALTER TABLE user
    ADD CONSTRAINT uc_user_pesel UNIQUE (pesel);

CREATE TABLE category
(
    id            BIGINT AUTO_INCREMENT NOT NULL,
    name          VARCHAR(255)          NULL,
    `description` VARCHAR(255)          NULL,
    CONSTRAINT pk_category PRIMARY KEY (id)
);

ALTER TABLE category
    ADD CONSTRAINT uc_category_name UNIQUE (name);

CREATE TABLE asset
(
    id            BIGINT AUTO_INCREMENT NOT NULL,
    name          VARCHAR(255)          NULL,
    `description` VARCHAR(255)          NULL,
    serial_number VARCHAR(255)          NULL,
    category_id   BIGINT                NULL,
    CONSTRAINT pk_asset PRIMARY KEY (id)
);

ALTER TABLE asset
    ADD CONSTRAINT uc_asset_serialnumber UNIQUE (serial_number);

ALTER TABLE asset
    ADD CONSTRAINT FK_ASSET_ON_CATEGORY FOREIGN KEY (category_id) REFERENCES category (id);


CREATE TABLE assignment
(
    id       BIGINT AUTO_INCREMENT NOT NULL,
    start    datetime              NULL,
    end      datetime              NULL,
    user_id  BIGINT                NULL,
    asset_id BIGINT                NULL,
    CONSTRAINT pk_assignment PRIMARY KEY (id)
);

ALTER TABLE assignment
    ADD CONSTRAINT FK_ASSIGNMENT_ON_ASSET FOREIGN KEY (asset_id) REFERENCES asset (id);

ALTER TABLE assignment
    ADD CONSTRAINT FK_ASSIGNMENT_ON_USER FOREIGN KEY (user_id) REFERENCES user (id);